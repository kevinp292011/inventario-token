INSERT INTO product (reference, name ) values ('1', 'Portatil'), ('2', 'Teclado'), ('3', 'Mouse');
INSERT INTO rols(name) values('ROLE_USER'), ('ROLE_ADMIN');
INSERT INTO users(username, password, name, last_name, email, enabled) values('abivan', '$2a$10$ippaaB0DJ90l9PGy4EMk/OcUsCguXX/jkCp7odf5Hm4uL6ikCzxku', 'ivan', 'agudelo', 'abivan@misena.edu.co', true);
INSERT INTO users(username, password, name, last_name, email, enabled) values('ybravo', '$2a$10$5erVB3zX4q1ysB67O9Jw2u7AnO2rZe8lyaATJdlZkWCMsl7rsXM2m', 'yohon', 'bravo', 'ybravo@misena.edu.co', true);
INSERT INTO user_has_rol values (1,1), (1,2);
INSERT INTO user_has_rol values (2,1);
