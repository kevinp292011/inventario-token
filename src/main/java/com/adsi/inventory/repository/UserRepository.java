package com.adsi.inventory.repository;

import com.adsi.inventory.domain.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users, Long> {

    public Users findByUsername(String userName);
}
