package com.adsi.inventory.auth;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
//para dar permisos a las rutas
    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers(HttpMethod.GET, "/api/product", "/api/users").permitAll()
                .antMatchers(HttpMethod.GET, "/api/product/{reference}").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.POST, "/api/product").hasAnyRole("ADMIN")
                .anyRequest().authenticated();
    }
}
