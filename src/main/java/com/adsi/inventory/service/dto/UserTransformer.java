package com.adsi.inventory.service.dto;

import com.adsi.inventory.domain.Users;

public class UserTransformer {

    public static UserDTO getUsersDTOFromUsers(Users users){
        if (users == null){
            return null;
        }

        UserDTO dto = new UserDTO();

        dto.setId(users.getId());
        dto.setUsername(users.getUsername());
        dto.setName(users.getName());
        dto.setLastName(users.getLastName());
        dto.setEmail(users.getEmail());
        dto.setPassword(users.getPassword());
        dto.setEnabled(users.getEnabled());
        dto.setRols(users.getRols());

        return dto;
    }

    public static Users getUsersFromUsersDTO(UserDTO dto) {

        if (dto == null) {
            return null;
        }

        Users users = new Users();

        users.setId(dto.getId());
        users.setUsername(dto.getUsername());
        users.setName(dto.getName());
        users.setLastName(dto.getLastName());
        users.setEmail(dto.getEmail());
        users.setPassword(dto.getPassword());
        users.setEnabled(dto.getEnabled());
        users.setRols(dto.getRols());

        return users;
    }

}
