package com.adsi.inventory.service.error;

import com.sun.istack.Nullable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ObjectNotFoundExeption extends RuntimeException{

    public ObjectNotFoundExeption(@Nullable String message){
        super(message);
    }
}
