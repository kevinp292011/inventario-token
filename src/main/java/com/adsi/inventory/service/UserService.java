package com.adsi.inventory.service;

import com.adsi.inventory.domain.Users;
import com.adsi.inventory.service.dto.UserDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

public interface UserService {

    List<UserDTO> getAll();

    UserDTO save(UserDTO userDTO);

    UserDTO update(UserDTO userDTO);

    UserDTO getById (Long id);

    UserDetails findByUserName(String userName);
}
