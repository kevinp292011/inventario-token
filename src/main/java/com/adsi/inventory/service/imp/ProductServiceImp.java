package com.adsi.inventory.service.imp;

import com.adsi.inventory.domain.Product;
import com.adsi.inventory.repository.ProductRepository;
import com.adsi.inventory.service.ProductService;
import com.adsi.inventory.service.dto.ProductDTO;
import com.adsi.inventory.service.dto.ProductTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImp implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<ProductDTO> getAll() {
        return productRepository.findAll().stream()
                .map(ProductTransformer::getProductDTOFromProduct)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDTO save(ProductDTO productDTO) {
        /*
        Product product = ProductTransformer.getProductFromProductDTO(productDTO);
        Product result = productRepository.save(product);
        return ProductTransformer.getProductDTOFromProduct(result);
         */
        return ProductTransformer.getProductDTOFromProduct(productRepository.save(ProductTransformer.getProductFromProductDTO(productDTO)));
    }

    @Override
    public ProductDTO getByReference(String reference) {
        return ProductTransformer.getProductDTOFromProduct(productRepository.findByReference(reference).get());
    }
}
